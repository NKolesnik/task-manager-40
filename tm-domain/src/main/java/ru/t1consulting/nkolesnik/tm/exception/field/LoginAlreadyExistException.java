package ru.t1consulting.nkolesnik.tm.exception.field;

public final class LoginAlreadyExistException extends AbstractFieldException {

    public LoginAlreadyExistException() {
        super("Error! Login already exist...");
    }

}
