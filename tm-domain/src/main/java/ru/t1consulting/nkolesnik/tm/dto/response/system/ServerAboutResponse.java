package ru.t1consulting.nkolesnik.tm.dto.response.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.response.AbstractResponse;

@Getter
@Setter
public final class ServerAboutResponse extends AbstractResponse {

    @Nullable
    private String email;

    @Nullable
    private String name;

}
