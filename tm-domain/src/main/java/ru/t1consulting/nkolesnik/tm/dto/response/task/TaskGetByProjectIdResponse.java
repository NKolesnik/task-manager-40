package ru.t1consulting.nkolesnik.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.response.AbstractResponse;
import ru.t1consulting.nkolesnik.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaskGetByProjectIdResponse extends AbstractResponse {

    @Nullable
    private List<Task> tasks;

    public TaskGetByProjectIdResponse(@Nullable final List<Task> tasks) {
        this.tasks = tasks;
    }

}