package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    void add(@Nullable Task task);

    void add(@Nullable String userId, @Nullable Task task);

    void add(@Nullable Collection<Task> tasks);

    void add(@Nullable String userId, @Nullable Collection<Task> tasks);

    void set(@Nullable Collection<Task> tasks);

    void set(@Nullable String userId, @Nullable Collection<Task> tasks);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    long getSize();

    long getSize(@Nullable String userId);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAll(@Nullable String userId);

    @NotNull
    List<Task> findAll(@Nullable Comparator<Task> comparator);

    @NotNull
    List<Task> findAll(@Nullable Sort sort);

    @NotNull
    List<Task> findAll(@Nullable String userId, @Nullable Comparator<Task> comparator);

    @NotNull
    List<Task> findAll(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    Task findById(@Nullable String id);

    @Nullable
    Task findById(@Nullable String userId, @Nullable String id);

    boolean existsById(@Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    void update(@Nullable Task task);

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    void changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void clear();

    void clear(@Nullable String userId);

    void remove(@Nullable Task task);

    void remove(@Nullable String userId, @Nullable Task task);

    void removeById(@Nullable String id);

    void removeById(@Nullable String userId, @Nullable String id);

    void removeByProjectId(@Nullable String userId, @Nullable String projectId);

}
