package ru.t1consulting.nkolesnik.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

public interface IConnectionService {

    @NotNull SqlSessionFactory getSqlSessionFactory();

    @NotNull SqlSession getSqlSession();

}

