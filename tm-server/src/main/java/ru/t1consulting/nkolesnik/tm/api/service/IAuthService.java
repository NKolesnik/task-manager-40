package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.model.Session;
import ru.t1consulting.nkolesnik.tm.model.User;

public interface IAuthService {

    @NotNull
    Session validateToken(@Nullable String token);

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

}
