package ru.t1consulting.nkolesnik.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.repository.IRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IService;
import ru.t1consulting.nkolesnik.tm.model.AbstractModel;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;


    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected SqlSession getSqlSession() {
        return connectionService.getSqlSession();
    }

}
