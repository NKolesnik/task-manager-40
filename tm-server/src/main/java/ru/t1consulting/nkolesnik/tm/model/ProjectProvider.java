package ru.t1consulting.nkolesnik.tm.model;

import org.mybatis.dynamic.sql.SqlColumn;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;

import java.util.Date;

public class ProjectProvider {

    public static final ProjectSqlTable projects = new ProjectSqlTable();

    public static final SqlColumn<String> id = projects.id;

    public static final SqlColumn<String> userId = projects.userId;

    public static final SqlColumn<String> name = projects.name;

    public static final SqlColumn<String> description = projects.description;

    public static final SqlColumn<Status> status = projects.status;

    public static final SqlColumn<Date> created = projects.created;

    public static final SqlColumn<Date> dateBegin = projects.dateBegin;

    public static final SqlColumn<Date> dateEnd = projects.dateEnd;


}
