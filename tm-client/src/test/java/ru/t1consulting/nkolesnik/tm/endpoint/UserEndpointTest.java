package ru.t1consulting.nkolesnik.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IAuthEndpoint;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IUserEndpoint;
import ru.t1consulting.nkolesnik.tm.dto.request.task.TaskGetByIdRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.user.*;
import ru.t1consulting.nkolesnik.tm.dto.response.task.TaskGetByIdResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.user.*;
import ru.t1consulting.nkolesnik.tm.marker.SoapCategory;
import ru.t1consulting.nkolesnik.tm.model.User;
import ru.t1consulting.nkolesnik.tm.util.HashUtil;

import java.util.UUID;

@Category(SoapCategory.class)
public class UserEndpointTest {

    @NotNull
    private static final String ADMIN_USER_LOGIN = "admin";
    @NotNull
    private static final String ADMIN_USER_PASSWORD = "admin";
    @NotNull
    private static final String TEST_USER_LOGIN = "test";
    @NotNull
    private static final String TEST_USER_PASSWORD = "test";
    @NotNull
    private static final String USER_EMAIL = "admin@admin.email.ru";
    @NotNull
    private static final String NEW_USER_LOGIN = "admin1";
    @NotNull
    private static final String NEW_USER_PASSWORD = "admin1";
    @NotNull
    private static final String NEW_USER_EMAIL = "admin1@admin.email.ru";
    @NotNull
    private static final String BAD_TOKEN = UUID.randomUUID().toString();
    @Nullable
    private static final String NULL_TOKEN = null;
    @Nullable
    private static final String NULL_PASSWORD = null;
    @Nullable
    private static final String NULL_EMAIL = null;
    @Nullable
    private static final String NULL_LOGIN = null;
    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance();
    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();
    @Nullable
    private String adminToken;

    @Nullable
    private String testToken;

    @Before
    public void setup() {
        @NotNull final UserLoginRequest adminLoginRequest = new UserLoginRequest(ADMIN_USER_LOGIN, ADMIN_USER_PASSWORD);
        adminToken = authEndpoint.login(adminLoginRequest).getToken();
        @NotNull final UserLoginRequest testLoginRequest = new UserLoginRequest(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        testToken = authEndpoint.login(testLoginRequest).getToken();
    }

    @After
    public void cleanup() {
        @NotNull final UserLogoutRequest adminLogoutRequest = new UserLogoutRequest(adminToken);
        authEndpoint.logout(adminLogoutRequest);
        @NotNull final UserLogoutRequest testLogoutRequest = new UserLogoutRequest(testToken);
        authEndpoint.logout(testLogoutRequest);
    }

    @Test
    public void registry() {
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(new UserRegistryRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(new UserRegistryRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(new UserRegistryRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(new UserRegistryRequest(testToken)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(new UserRegistryRequest(adminToken)));
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.registryUser(
                        new UserRegistryRequest(adminToken, NULL_LOGIN, NEW_USER_PASSWORD, NEW_USER_EMAIL)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.registryUser(
                        new UserRegistryRequest(adminToken, NEW_USER_LOGIN, NULL_PASSWORD, NEW_USER_EMAIL)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.registryUser(
                        new UserRegistryRequest(adminToken, NEW_USER_LOGIN, NEW_USER_PASSWORD, NULL_EMAIL)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.registryUser(
                        new UserRegistryRequest(adminToken, NEW_USER_LOGIN, NEW_USER_PASSWORD, USER_EMAIL)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.registryUser(
                        new UserRegistryRequest(adminToken, ADMIN_USER_LOGIN, NEW_USER_PASSWORD, NEW_USER_EMAIL)
                )
        );
        @NotNull final UserRegistryRequest registryRequest =
                new UserRegistryRequest(adminToken, NEW_USER_LOGIN, NEW_USER_PASSWORD, NEW_USER_EMAIL);
        @NotNull final UserRegistryResponse registryResponse = userEndpoint.registryUser(registryRequest);
        Assert.assertNotNull(registryResponse);
        Assert.assertNotNull(registryResponse.getUser());
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken, NEW_USER_LOGIN);
        userEndpoint.removeUser(removeRequest);
    }

    @Test
    public void remove() {
        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest();
        registryRequest.setLogin(NEW_USER_LOGIN);
        registryRequest.setPassword(NEW_USER_PASSWORD);
        registryRequest.setEmail(NEW_USER_EMAIL);
        userEndpoint.registryUser(registryRequest);
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(new UserRemoveRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(new UserRemoveRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(new UserRemoveRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(new UserRemoveRequest(testToken)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(new UserRemoveRequest(testToken, NEW_USER_LOGIN)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(new UserRemoveRequest(adminToken, NULL_LOGIN)));
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken, NEW_USER_LOGIN);
        Assert.assertNotNull(userEndpoint.removeUser(removeRequest));
    }

    @Test
    public void lockUser() {
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(new UserLockRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(new UserLockRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(new UserLockRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(new UserLockRequest(testToken)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(new UserLockRequest(adminToken, NULL_LOGIN)));
        @NotNull final UserLockRequest lockRequest = new UserLockRequest(adminToken, TEST_USER_LOGIN);
        @NotNull final UserLockResponse lockResponse = userEndpoint.lockUser(lockRequest);
        Assert.assertNotNull(lockResponse);
        Assert.assertTrue(lockResponse.getSuccess());
        @NotNull final UserUnlockRequest unlockRequest = new UserUnlockRequest(adminToken, TEST_USER_LOGIN);
        userEndpoint.unlockUser(unlockRequest);
    }

    @Test
    public void unlockUser() {
        @NotNull final UserLockRequest lockRequest = new UserLockRequest();
        lockRequest.setLogin(TEST_USER_LOGIN);
        lockRequest.setToken(adminToken);
        userEndpoint.lockUser(lockRequest);
        @NotNull final UserLockResponse lockResponse = userEndpoint.lockUser(lockRequest);
        Assert.assertNotNull(lockResponse);
        Assert.assertTrue(lockResponse.getSuccess());
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(new UserUnlockRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(new UserUnlockRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(new UserUnlockRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(new UserUnlockRequest(testToken)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(new UserUnlockRequest(adminToken, NULL_LOGIN)));
        @NotNull final UserUnlockRequest unlockRequest = new UserUnlockRequest(adminToken, TEST_USER_LOGIN);
        @NotNull final UserUnlockResponse unlockResponse = userEndpoint.unlockUser(unlockRequest);
        Assert.assertNotNull(unlockResponse);
        Assert.assertTrue(unlockResponse.getSuccess());
    }

    @Test
    public void changePassword() {
        @NotNull final UserProfileRequest profileRequest = new UserProfileRequest(testToken);
        @NotNull final UserProfileResponse profileResponse = userEndpoint.showProfileUser(profileRequest);
        Assert.assertNotNull(profileResponse);
        Assert.assertNotNull(profileResponse.getUser());
        @Nullable final String oldPassword = profileResponse.getUser().getPasswordHash();
        Assert.assertThrows(Exception.class, () -> userEndpoint.changePassword(new UserChangePasswordRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.changePassword(new UserChangePasswordRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.changePassword(new UserChangePasswordRequest(BAD_TOKEN)));
        Assert.assertThrows(
                Exception.class,
                () -> userEndpoint.changePassword(new UserChangePasswordRequest(testToken, NULL_PASSWORD))
        );
        @NotNull final UserChangePasswordRequest changePasswordRequest =
                new UserChangePasswordRequest(testToken, NEW_USER_PASSWORD);
        userEndpoint.changePassword(changePasswordRequest);
        @NotNull final UserProfileRequest userProfileRequest = new UserProfileRequest(testToken);
        @NotNull final UserProfileResponse userProfileResponse = userEndpoint.showProfileUser(userProfileRequest);
        Assert.assertNotNull(userProfileResponse);
        Assert.assertNotNull(userProfileResponse.getUser());
        @NotNull final User repositoryUser = userProfileResponse.getUser();
        Assert.assertNotEquals(oldPassword, repositoryUser.getPasswordHash());
        @NotNull final String secret = "951753";
        @NotNull final Integer iteration = 3751;
        Assert.assertEquals(
                HashUtil.salt(NEW_USER_PASSWORD, secret, iteration),
                repositoryUser.getPasswordHash()
        );
        changePasswordRequest.setPassword(TEST_USER_PASSWORD);
        userEndpoint.changePassword(changePasswordRequest);
    }

    @Test
    public void updateUser() {
        @NotNull final UserProfileRequest profileRequest = new UserProfileRequest();
        profileRequest.setToken(testToken);
        @NotNull final UserProfileResponse profileResponse = userEndpoint.showProfileUser(profileRequest);
        Assert.assertNotNull(profileResponse);
        Assert.assertNotNull(profileResponse.getUser());
        @Nullable final String oldFirstName = profileResponse.getUser().getFirstName();
        @Nullable final String oldMiddleName = profileResponse.getUser().getMiddleName();
        @Nullable final String oldLastName = profileResponse.getUser().getLastName();
        @NotNull final String newFirstName = "TTT";
        @NotNull final String newMiddleName = "EEE";
        @NotNull final String newLastName = "WWW";
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUser(new UserUpdateRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUser(new UserUpdateRequest(NULL_TOKEN)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUser(new UserUpdateRequest(BAD_TOKEN)));
        @NotNull final UserUpdateRequest updateRequest =
                new UserUpdateRequest(testToken, newFirstName, newMiddleName, newLastName);
        userEndpoint.updateUser(updateRequest);
        @NotNull final UserProfileRequest userProfileRequest = new UserProfileRequest(testToken);
        @NotNull final UserProfileResponse userProfileResponse = userEndpoint.showProfileUser(userProfileRequest);
        Assert.assertNotNull(userProfileResponse);
        Assert.assertNotNull(userProfileResponse.getUser());
        @NotNull final User repositoryUser = userProfileResponse.getUser();
        Assert.assertNotEquals(oldFirstName, repositoryUser.getFirstName());
        Assert.assertNotEquals(oldMiddleName, repositoryUser.getMiddleName());
        Assert.assertNotEquals(oldLastName, repositoryUser.getLastName());
        Assert.assertEquals(newFirstName, repositoryUser.getFirstName());
        Assert.assertEquals(newMiddleName, repositoryUser.getMiddleName());
        Assert.assertEquals(newLastName, repositoryUser.getLastName());
        updateRequest.setFirstName(oldFirstName);
        updateRequest.setMiddleName(oldMiddleName);
        updateRequest.setLastName(oldLastName);
        userEndpoint.updateUser(updateRequest);
    }

    @Test
    public void showProfileUser() {
        Assert.assertThrows(Exception.class, () -> userEndpoint.showProfileUser(new UserProfileRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.showProfileUser(new UserProfileRequest(BAD_TOKEN)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.showProfileUser(new UserProfileRequest(NULL_TOKEN)));
        @NotNull final UserProfileRequest profileRequest = new UserProfileRequest(testToken);
        @NotNull final UserProfileResponse profileResponse = userEndpoint.showProfileUser(profileRequest);
        Assert.assertNotNull(profileResponse);
        Assert.assertNotNull(profileResponse.getUser());
        Assert.assertEquals(TEST_USER_LOGIN, profileResponse.getUser().getLogin());
    }

}
